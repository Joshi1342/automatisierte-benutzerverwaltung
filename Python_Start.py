import logging
import mysql.connector
import os
from pathlib import Path

#------ Global Variables ------#

#-- Group IDs --#

# gid_1 = Admin
# gid_2 = MySQL, Docker, Apache
# gid_3 = Standard-Benutzer

gid_1 = 1
gid_2 = 2
gid_3 = 3

#-- Directory IDs --#

# did_1 = Gesamtsystem
# did_2 = Home-Docker
# did_3 = Home

did_1 = 1
did_2 = 2
did_3 = 3

#-- Access IDs --#

# aid_1 = Sudo
# aid_2 = Keine

aid_1 = 1
aid_2 = 2

#-- Login IDs --#

# 1 = Keine
# 2 = "SSH"
# 3 = "Passwort"
# 4 = "Passwort, SSH"

lid_1 = 1
lid_2 = 2
lid_3 = 3
lid_4 = 4

#------ Logging ------#

Logdir_path = os.path.dirname(os.path.realpath(__file__))
Logdir_name = "/logs"
Logdir = Logdir_path + Logdir_name
Logfile = Path(Logdir + "/log.txt")

#-- Basic Config --#

logging.basicConfig(filename = "log.txt", filemode = 'a', format = '%(asctime)s - %(levelname)s - %(message)s', level = logging.info)

#-- Log-file --#

# Creates Log-directory if it doesn't exist

if os.path.exists(Logdir):
    pass
else:
    os.mkdir(Logdir)


# Creates Logfile if it doesn't exist

if Logfile.is_file():
    pass
else:
    open(Logfile, mode = 'a')


def DBConnector():

    mydb = mysql.connector.connect(
        host="",
        user=""    
    )

    global mydb

class Abfrage:
    
    def __init__(self, sql):
        self.sql = sql

    def Query():

        DBConnector()
        mycursor = mydb.cursor()

        mycursor.execute(sql)
        myresult = mycursor.fetchall()

class Längenabgleich:

    def __init__(self, var_to_check, smaller_than, larger_than):
        self.var_to_check = var_to_check
        self.larger_than = larger_than
        self.smaller_than = smaller_than

    def var_larger(var_to_check, smaller_than):
        if smaller_than > var_to_check:
            pass
        else:
            logging.error(var_to_check + "is smaller than" + smaller than + "!")
            print(var_to_check + "is smaller than" + smaller than + "!")
            sys.exit()
    
    def var_smaller(var_to_check, larger_than):
        if var_to_check > larger_than:
            pass
        else:
            logging.error(var_to_check + "is lager than" + bigger_than + "!")
            print(var_to_check + "is lager than" + bigger_than + "!")
            sys.exit()

    def var_between(var_to_check, smaller_than, larger_than):
        if var_to_check < smaller_than:
            logging.error(var_to_check + "is smaller than" + smaller_than + "!")
            print(var_to_check + "is smaller than" + smaller_than + "!")
            sys.exit()
        elif larger_than > var_to_check:
            logging.error(var_to_check + "is larger than" + bigger_than + "!")
            print(var_to_check + "is smaller than" + bigger_than + "!")
            sys.exit()
        else:
            pass
    


def Abfrage(sql):

    DBConnector()
    mycursor = mydb.cursor()

    mycursor.execute(sql)
    myresult = mycursor.fetchall()

    global myresult

def main():

    Abfrage("SELECT Username, SSH_key, User_Password, Group_ID, Directory_ID, Access_ID, Login_ID FROM User")
    
    Username = input("Bitte einen Usernamen aus 15 Zeichen definieren")
    SSH_Key = input("SSH Key bitte eintragen")
    User_Password = input("User Passwort bitte eintragen")
    Group_ID = input("Gruppenrechte zwischen 1-3 eintragen")
    Directory_ID = input("Verzeichnisrechte zwischen 1-3 eintragen")
    Access_ID = input("Sudorechte zwischen 1 und 2 eintragen")
    Login_ID = input("Loginrechte zwischen 1-4 eintragen")
 
    #------ Userüberprüfung ------#

    Username_length = len(Username) 
    if Username_length <= 15:
        pass
    else:
        logging.error('Achte auf die Zeichenlänge!')
        print("Achte auf die Zeichenlänge!")
        sys.exit()

    SSH_Key_lenght = len(SSH_Key) 
    if SSH_Key_length >0:
        pass
    else:
        logging.error('Bitte SSH Key eintragen!')
        print("Bitte SSH Key eintragen!")
        sys.exit()

    User_Password_lenght = len(User_Password) 
    if User_Password_length >0:
        pass
    else:
        logging.error('Bitte Passwort eintragen!')
        print("Bitte Passwort eintragen!")
        sys.exit()

    Group_ID_number = len(Group_ID) 
    if 1 < Group_ID_number <= 3
        pass
    else:
        logging.error('Bitte nur eine Zahl zwischen 1-3 eintragen!')
        print("Bitte nur eine Zahl zwischen 1-3 eintragen!")
        sys.exit()

    Directory_ID_number = len(Directory_ID) 
    if 1 < Directory_ID_number <= 3
        pass
    else:
        logging.error('Bitte nur eine Zahl zwischen 1-3 eintragen!')
        print("Bitte nur eine Zahl zwischen 1-3 eintragen!")
        sys.exit()

    Access_ID_number = len(Access_ID) 
    if 1 < Access_ID_number <= 2
        pass
    else:
        logging.error('Bitte nur eine Zahl zwischen 1 und 2 eintragen!')
        print("Bitte nur eine Zahl zwischen 1 und 2 eintragen!")
        sys.exit()

    Login_ID_number = len(Login_ID) 
    if 1 < Access_ID_number <= 4
        pass
    else:
        logging.error('Bitte nur eine Zahl zwischen 1-4 eintragen!')
        print("Bitte nur eine Zahl zwischen 1-4 eintragen!")
        sys.exit()

    # if Username_Länge <= 10:
    #     pass
    # else:
    #     Fick dich

    # os.system("")


    #------ Userüberprüfung ------#

    #-- Get Usernames --#

    # lslogins -un |grep USER --> Zeigt alle User an

    # jdatu@DESKTOP-0ASPSJH:~$ lslogins -un |grep USER
    # USER="root"
    # USER="jdatu"
    # USER="test"

    # Usernamen daraus extrahieren und in Array packen

    # test=$(lslogins -un |grep USER | sed -e 's/USER=//g' | sed -e 's/"//g') --> sed tauscht '*USER=' und '"' gegen ein leeres Zeichen aus
    # declare -a test2
    # jdatu@DESKTOP-0ASPSJH:~$ test2 = $(test)

    # jdatu@DESKTOP-0ASPSJH:~$ echo ${test2[@]}
    # root jdatu test
    # jdatu@DESKTOP-0ASPSJH:~$ echo ${test2[2]}
    # test
    # jdatu@DESKTOP-0ASPSJH:~$ echo ${test2[1]}
    # jdatu
    # jdatu@DESKTOP-0ASPSJH:~$ echo ${test2[0]}
    # root

    #-- SSH Key Überprüfung --#


    #------ Erstellung Linux Befehl ------#



if __name__ == "__main__":
    main()
